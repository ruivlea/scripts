#!/bin/sh -e

# statusbar for dwm

while true; do
    xsetroot -name "$(date +'%H:%M %Z %a %d-%m-%Y')"
    sleep 2
done
